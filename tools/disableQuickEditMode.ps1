Add-Type -MemberDefinition @"
[DllImport("kernel32.dll", SetLastError=true)]
public static extern bool SetConsoleMode(IntPtr hConsoleHandle, int mode);
[DllImport("kernel32.dll", SetLastError=true)]
public static extern IntPtr GetStdHandle(int handle);
"@ -Namespace Win32 -Name NativeMethods
$Handle = [Win32.NativeMethods]::GetStdHandle(-10)
return [Win32.NativeMethods]::SetConsoleMode($Handle, 0x0080)
